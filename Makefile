CC = xelatex

SRC_DIR = src

CV_DIR_ENG = src/cv-eng
CV_DIR_CAT = src/cv-cat

CV_SRCS_ENG = $(shell find $(CV_DIR_ENG) -name '*.tex')
CV_SRCS_CAT = $(shell find $(CV_DIR_CAT) -name '*.tex')

OUTPUT_DIR=.
OUTPUT_NAME=cv.pdf

cv-eng: $(SRC_DIR)/cv-eng.tex $(CV_SRCS_ENG)
	$(CC) -output-directory=$(SRC_DIR) $<
	mv $(SRC_DIR)/cv-eng.pdf $(OUTPUT_DIR)/$(OUTPUT_NAME)

cv-cat: $(SRC_DIR)/cv-cat.tex $(CV_SRCS_CAT)
	$(CC) -output-directory=$(SRC_DIR) $<
	mv $(SRC_DIR)/cv-cat.pdf $(OUTPUT_DIR)/$(OUTPUT_NAME)

clean:
	rm -rf $(SRC_DIR)/*.aux $(SRC_DIR)/*.out $(SRC_DIR)/*.log $(SRC_DIR)/*~

veryclean:
	make clean
	rm -rf $(OUTPUT_DIR)/$(OUTPUT_NAME)
